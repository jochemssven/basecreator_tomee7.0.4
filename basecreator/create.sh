#!/bin/sh

NR=$1

cp -r BASE_CLEAN BASE${NR}

find BASE${NR}/setenv.bat -type f -exec sed -i "s/{BASENR}/${NR}/g" {} +
find BASE${NR}/setenv.sh -type f -exec sed -i "s/{BASENR}/${NR}/g" {} +
find BASE${NR}/conf/ -type f -exec sed -i "s/{BASENR}/${NR}/g" {} +

exit 0
