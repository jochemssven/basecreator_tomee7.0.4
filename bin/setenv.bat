set CATALINA_BASE=%CATALINA_HOME%\%TOMEE_BASE%
set CATALINA_OPTS=%CATALINA_OPTS% -server -Xms512m -Xmx1024m -Xss256k -XX:PermSize=512m -XX:MaxPermSize=1024m

rem set CLASSPATH=%CATALINA_HOME%/bin/log/jul-to-slf4j-1.7.5.jar;%CATALINA_HOME%/bin/log/log4j-1.2.17.jar;%CATALINA_HOME%/bin/log/slf4j-api-1.7.5.jar;%CATALINA_HOME%/bin/log/slf4j-log4j12-1.7.5.jar;%CATALINA_BASE%/conf/logging/

if exist %CATALINA_BASE%\setenv.bat (
  call %CATALINA_BASE%\setenv.bat
)