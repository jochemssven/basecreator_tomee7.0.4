cd %~dp0

if "%~1"=="stop" (
    catalina.bat stop
)
if "%~1"=="cleanup" (
    rd /S /Q ..\%TOMEE_BASE%\work
    md ..\%TOMEE_BASE%\work
)
if "%~1"=="start" (
    rd /S /Q ..\%TOMEE_BASE%\work
    md ..\%TOMEE_BASE%\work
    catalina.bat run
)
