cd $(dirname $0)

function stop() {
    ./catalina.sh stop
}
function cleanup() {
    rm -r ../${TOMEE_BASE}/work
    mkdir ../${TOMEE_BASE}/work
}
function start() {
    cleanup
    ./catalina.sh start
}

if [ "$1" = "stop" ]; then
    stop
fi
if [ "$1" = "cleanup" ]; then
    cleanup
fi
if [ "$1" = "start" ]; then
    start
fi

cd -
