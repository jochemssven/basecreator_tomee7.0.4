export CATALINA_BASE="$CATALINA_HOME/$TOMEE_BASE"
export CATALINA_OPTS="$CATALINA_OPTS -server -Xms512m -Xmx1024m -Xss256k -XX:PermSize=512m -XX:MaxPermSize=1024m"

if [ -f "$CATALINA_BASE/setenv.sh" ]; then
  $CATALINA_BASE/setenv.sh
fi